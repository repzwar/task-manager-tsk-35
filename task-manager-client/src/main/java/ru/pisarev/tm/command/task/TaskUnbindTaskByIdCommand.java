package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.Task;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

import static ru.pisarev.tm.util.TerminalUtil.incorrectValue;

public class TaskUnbindTaskByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Task taskUpdated = serviceLocator.getTaskEndpoint().unbindTaskById(getSession(), taskId);
        if (taskUpdated == null) incorrectValue();
    }
}
