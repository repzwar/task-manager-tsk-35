package ru.pisarev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.endpoint.Session;
import ru.pisarev.tm.endpoint.SessionEndpoint;
import ru.pisarev.tm.endpoint.SessionEndpointService;
import ru.pisarev.tm.endpoint.User;
import ru.pisarev.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Nullable
    private Session session;

    @Before
    public void before() {
        session = sessionEndpoint.open("user", "user");
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void openIncorrect() {
        @NotNull final Session session = sessionEndpoint.open("test", "test");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void registerIncorrect() {
        @NotNull final Session session = sessionEndpoint.register("user", "user", "email");
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUser() {
        @NotNull final User user = sessionEndpoint.updateUser(session, "f", "l", "m");
        Assert.assertNotNull(user);
        Assert.assertEquals("f", user.getFirstName());
        Assert.assertEquals("l", user.getLastName());
        Assert.assertEquals("m", user.getMiddleName());
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void close() {
        sessionEndpoint.close(this.session);
        sessionEndpoint.updateUser(session, "f", "l", "m");
    }

}
