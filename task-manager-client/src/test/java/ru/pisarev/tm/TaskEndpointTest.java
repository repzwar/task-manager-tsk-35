package ru.pisarev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.pisarev.tm.endpoint.*;
import ru.pisarev.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Nullable
    private static Session session;

    @Nullable
    private Task task;

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.open("user", "user");
    }

    @Before
    public void before() {
        task = new Task();
        task.setName("Test");
        task.setId("1");
        taskEndpoint.addTask(session, task);
    }

    @After
    public void after() {
        taskEndpoint.removeTaskById(session, task.getId());
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.close(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Test", task.getName());

        @NotNull final Task taskById = taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        @NotNull final List<Task> tasks = taskEndpoint.findTaskAll(session);
        Assert.assertTrue(tasks.size() > 0);
    }


    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findAllByUserIdIncorrect() {
        taskEndpoint.findTaskAll(new Session());
    }

    @Test
    @Category(SoapCategory.class)
    public void findById() {
        @NotNull final Task task = taskEndpoint.findTaskById(session, this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskEndpoint.findTaskById(session, "34");
        Assert.assertNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdNull() {
        taskEndpoint.findTaskById(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIdIncorrectUser() {
        taskEndpoint.findTaskById(new Session(), this.task.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByName() {
        @NotNull final Task task = taskEndpoint.findTaskByName(session, "Test");
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskEndpoint.findTaskByName(session, "34");
        Assert.assertNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameNull() {
        taskEndpoint.findTaskByName(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByNameIncorrectUser() {
        taskEndpoint.findTaskByName(new Session(), this.task.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void findByIndex() {
        @NotNull final Task task = taskEndpoint.findTaskByIndex(session, 0);
        Assert.assertNotNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrect() {
        taskEndpoint.findTaskByIndex(session, 34);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexNull() {
        taskEndpoint.findTaskByIndex(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void findByIndexIncorrectUser() {
        taskEndpoint.findTaskByIndex(new Session(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        taskEndpoint.removeTaskById(session, task.getId());
        Assert.assertNull(taskEndpoint.findTaskById(session, task.getId()));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdNull() {
        Assert.assertNull(taskEndpoint.removeTaskById(session, null));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdIncorrect() {
        @NotNull final Task task = taskEndpoint.removeTaskById(session, "34");
        Assert.assertNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIdIncorrectUser() {
        taskEndpoint.removeTaskById(new Session(), this.task.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIndex() {
        @NotNull final Task task = taskEndpoint.removeTaskByIndex(session, 0);
        Assert.assertNotNull(task);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrect() {
        taskEndpoint.removeTaskByIndex(session, 34);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexNull() {
        taskEndpoint.removeTaskByIndex(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByIndexIncorrectUser() {
        taskEndpoint.removeTaskByIndex(new Session(), 0);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByName() {
        @NotNull final Task task = taskEndpoint.removeTaskByName(session, "Test");
        Assert.assertNotNull(task);
    }

    @Category(SoapCategory.class)
    @Test
    public void removeByNameIncorrect() {
        Assert.assertNull(taskEndpoint.removeTaskByName(session, "34"));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameNull() {
        taskEndpoint.removeTaskByName(session, null);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void removeByNameIncorrectUser() {
        taskEndpoint.removeTaskByName(new Session(), this.task.getName());
    }

}
