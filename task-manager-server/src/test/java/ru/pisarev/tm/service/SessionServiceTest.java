package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.model.Session;
import ru.pisarev.tm.repository.SessionRepository;
import ru.pisarev.tm.repository.UserRepository;

import java.util.List;

public class SessionServiceTest {

    @Nullable
    private SessionService sessionService;

    @Nullable
    private Session session;

    @Before
    public void before() {
        @NotNull final UserService userService = new UserService(new UserRepository(), new PropertyService());
        userService.add("user", "user");
        sessionService = new SessionService(
                new SessionRepository(),
                userService,
                new PropertyService());
        @NotNull final Session session = new Session();
        session.setUserId("userId");
        this.session = sessionService.add(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("userId", session.getUserId());

        @NotNull final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionService.findById("34");
        Assert.assertNull(session);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        sessionService.findById(null);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionService.findAllByUserId(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionService.findAllByUserId("34");
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdNull() {
        @NotNull final List<Session> session = sessionService.findAllByUserId(null);
        Assert.assertNull(session);
    }

    @Test
    public void remove() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        sessionService.removeById(null);
    }

    @Test
    public void removeById() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        sessionService.removeById(null);
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Session session = sessionService.removeById("34");
        Assert.assertNull(session);
    }

    @Test
    public void close() {
        sessionService.close(session);
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test
    public void closeAllByUserId() {
        sessionService.closeAllByUserId(session.getUserId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test(expected = AccessDeniedException.class)
    public void validateIncorrect() {
        sessionService.validate(session);
    }

    @Test
    public void open() {
        @NotNull final Session session = sessionService.open("user", "user");
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open("user", "use");
    }

    @Test
    public void validate() {
        @NotNull final Session session = sessionService.open("user", "user");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @NotNull final Session session = sessionService.open("user", "user");
        session.setSignature("7");
        sessionService.validate(session);
    }

}