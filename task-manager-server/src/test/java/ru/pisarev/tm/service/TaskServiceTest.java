package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.system.IndexIncorrectException;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.repository.TaskRepository;

import java.util.List;

public class TaskServiceTest {

    @Nullable
    private TaskService taskService;

    @Nullable
    private Task task;

    @Before
    public void before() {
        taskService = new TaskService(new TaskRepository());
        task = taskService.add("testUser", new Task("Task"));
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskService.findAll("testUser");
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskService.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final Task task = taskService.findById("testUser", this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Task task = taskService.findById("testUser", "34");
        Assert.assertNull(task);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Task task = taskService.findById("testUser", null);
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskService.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        Assert.assertNull(taskService.removeById(null));
    }

    @Test
    public void findByName() {
        @NotNull final Task task = taskService.findByName("testUser", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Task task = taskService.findByName("testUser", "34");
        Assert.assertNull(task);
    }

    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final Task task = taskService.findByName("testUser", null);
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskService.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final Task task = taskService.findByIndex("testUser", 0);
        Assert.assertNotNull(task);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrect() {
        @NotNull final Task task = taskService.findByIndex("testUser", 34);
        Assert.assertNull(task);
    }

    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final Task task = taskService.findByIndex("testUser", null);
        Assert.assertNull(task);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrectUser() {
        @NotNull final Task task = taskService.findByIndex("test", 0);
        Assert.assertNull(task);
    }

    @Test
    public void removeById() {
        taskService.removeById("testUser", task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        Assert.assertNull(taskService.removeById("testUser", null));
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Task task = taskService.removeById("testUser", "34");
        Assert.assertNull(task);
    }

    @Test
    public void removeByIdIncorrectUser() {
        @NotNull final Task task = taskService.removeById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeByIndex() {
        @NotNull final Task task = taskService.removeByIndex("testUser", 0);
        Assert.assertNotNull(task);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrect() {
        taskService.removeByIndex("testUser", 34);
    }

    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        taskService.removeByIndex("testUser", null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrectUser() {
        taskService.removeByIndex("test", 0);
    }

    @Test
    public void removeByName() {
        @NotNull final Task task = taskService.removeByName("testUser", "Task");
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByNameIncorrect() {
        @NotNull final Task task = taskService.removeByName("testUser", "34");
        Assert.assertNull(task);
    }

    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        taskService.removeByName("testUser", null);
    }

    @Test
    public void removeByNameIncorrectUser() {
        @NotNull final Task task = taskService.removeByName("test", this.task.getName());
        Assert.assertNull(task);
    }

}
