package ru.pisarev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.entity.IHasCreated;
import ru.pisarev.tm.api.entity.IHasName;
import ru.pisarev.tm.api.entity.IHasStartDate;
import ru.pisarev.tm.api.entity.IHasStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", Comparator.comparing(IHasName::getName)),
    CREATED("Sort by created", Comparator.comparing(IHasCreated::getCreated)),
    START_DATE("Sort by date start", Comparator.comparing(IHasStartDate::getStartDate)),
    STATUS("Sort by status", Comparator.comparing(IHasStatus::getStatus));

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
