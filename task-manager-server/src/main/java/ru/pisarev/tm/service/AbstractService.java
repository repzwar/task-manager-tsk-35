package ru.pisarev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    @NotNull
    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        @NotNull final Optional<Collection<E>> optional = Optional.ofNullable(collection);
        optional.ifPresent(repository::addAll);
    }

    @Nullable
    @Override
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Nullable
    @Override
    public E remove(@Nullable final E entity) {
        if (entity == null) return null;
        return repository.remove(entity);
    }
}
